﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace QJFile.Data
{
    using Microsoft.Win32;
    using System;
    using System.Configuration;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.IO;

    public partial class FileCenterEntities : DbContext
    {
        public FileCenterEntities()
            : base(GetEFConnctionString())
        {
        }

        private static string GetEFConnctionString()
        {
            ///坑爹的不能直接用字符串,C#会转码&quot
            string connectionString = ConfigurationManager.ConnectionStrings["FileCenterEntities"].ConnectionString;
            return connectionString;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AppSetting> AppSetting { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<Qycode> Qycode { get; set; }
        public virtual DbSet<Document> Document { get; set; }

        public virtual DbSet<userlog> userlog { get; set; }


    }
}
